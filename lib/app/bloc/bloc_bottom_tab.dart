
import 'dart:async';

enum NavBarItem { 
  HOME, 
  HISTORY,
}

var navs = [
  NavBarItem.HOME,
  NavBarItem.HISTORY
];

class BottomNavBarBloc{
  final StreamController<NavBarItem> _navBarController = StreamController<NavBarItem>.broadcast();

  NavBarItem defaultItem = NavBarItem.HOME;
  Stream<NavBarItem> get itemStream => _navBarController.stream;

  void pickItem(int i) => _navBarController.sink.add(navs[i]);
  
  void dispose() {
    _navBarController?.close();
  }
}
