import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BottomBarIOS extends StatelessWidget {
  final int currentIndex;
  final ValueChanged<int> onTap;
  final List<BottomNavigationBarItem> items;
  final Color inactiveColor;
  final Color activeColor;
  final double size;

  const BottomBarIOS({
    Key key,
    @required this.currentIndex,
    @required this.onTap,
    @required this.items,
    this.activeColor,
    this.inactiveColor,
    @required this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoTabBar(
      backgroundColor: Theme.of(context).primaryColor,
      inactiveColor: inactiveColor,
      activeColor: activeColor,
      iconSize: size,
      currentIndex: currentIndex,
      onTap: onTap,
      items: items,
    );
  }
}
