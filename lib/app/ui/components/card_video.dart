import 'dart:ui' as ui;
import 'package:cameralivestream/app/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ijkplayer/flutter_ijkplayer.dart'
;
class CardVideo extends StatefulWidget {

  final String title;
  final String url;
  final double height;
  final bool autoPlay;
  CardVideo({Key key, this.title, this.url, this.height, this.autoPlay = false}) : super(key: key);

  @override
  _CardVideoState createState() => _CardVideoState();
}

class _CardVideoState extends State<CardVideo> {
   IjkMediaController controller = IjkMediaController();

  ImageProvider imageProvider;
  @override
  void initState() {
    super.initState();
    controller.setNetworkDataSource(
      widget.url,
      autoPlay: widget.autoPlay
    );
     
  }

  @override
  void dispose() {
      controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 60, right: 60, top: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: BackdropFilter(
              filter: ui.ImageFilter.blur(sigmaX: 10.0),
              child: Container(
                height: widget.height,
                child: Center(
                  child: Stack(
                    children: <Widget>[
                      StreamBuilder<IjkStatus>(
                        stream: controller.ijkStatusStream,
                        initialData: controller.ijkStatus,
                        builder: (ctx, snapshot) {
                          if(snapshot.data == IjkStatus.complete || snapshot.data == IjkStatus.error){
                              controller.setNetworkDataSource(
                              widget.url,
                              autoPlay: true
                            );
                          }
                            
                          return IjkPlayer(
                            mediaController: controller,
                          );
                        }
                      ),
                      Positioned( 
                        top: 20,
                        left: 20,
                        child: StreamBuilder( 
                          stream: controller.playingStream,
                          initialData: controller?.isPlaying ?? false,
                          builder: (ctx, snapshot){
                            return !snapshot.data ?
                              Text(
                                widget.title,
                                style: MyStyleText.textStyleTitleVideo,
                              ) : SizedBox();
                          }, 
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}