import 'package:cameralivestream/app/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ijkplayer/flutter_ijkplayer.dart';
import 'package:flutube/flutube.dart';
import 'dart:ui' as ui;

class ItemListCardVideo extends StatefulWidget {
  final double height;
  final String urlThumnail;
  final String url;
  final String title; 


  ItemListCardVideo({
    Key key, 
    this.height, 
    this.urlThumnail, 
    this.url, 
    this.title
  }) : super(key:key);

  @override
  _ItemListCardVideoState createState() => _ItemListCardVideoState();
}

class _ItemListCardVideoState extends State<ItemListCardVideo> {
  IjkMediaController _controller = IjkMediaController();

  initVideo(){
    _controller.setNetworkDataSource(
      widget.url,
      autoPlay: false
    );
  }

@override
  void initState() {
    super.initState();
    initVideo();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: BackdropFilter(
              filter: ui.ImageFilter.blur(sigmaX: 10.0),
              child: Container(
                height: widget.height,
                width: MediaQuery.of(context).size.width * 0.6,
                child: Center(
                  child: StreamBuilder<IjkStatus>(
                    stream: _controller.ijkStatusStream,
                    initialData: _controller.ijkStatus,
                    builder: (ctx, snapshot) {
                      print(snapshot.data);
                      if(snapshot.data == IjkStatus.complete || snapshot.data == IjkStatus.error){
                          initVideo();
                      }
                      
                      return IjkPlayer(
                        mediaController: _controller,
                      );
                    }
                  )
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10, top: 10),
              child: Text(
                widget.title,
                style: MyStyleText.textStyleTitleVideo.copyWith(
                  color: Colors.black,
                  fontSize: 15
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}