import 'package:flutter/material.dart';

class MyColor {
  static const Color item_inactive            = Colors.grey;
  static const Color item_active              = Colors.black;
}