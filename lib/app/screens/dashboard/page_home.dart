import 'package:cameralivestream/app/ui/components/card_video.dart';
import 'package:flutter/material.dart';

class PageHome extends StatefulWidget {
  @override
  _PageHomeState createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  
  List<String> listVideo = [
    "http://media.camhub.ai/LiveApp/streams/qYKL0Rbr3uwJ1575964960369.m3u8",
    "http://media.camhub.ai/LiveApp/streams/qYKL0Rbr3uwJ1575964960369.m3u8",
    "http://media.camhub.ai/LiveApp/streams/qYKL0Rbr3uwJ1575964960369.m3u8",
    "http://media.camhub.ai/LiveApp/streams/qYKL0Rbr3uwJ1575964960369.m3u8",
    "http://media.camhub.ai/LiveApp/streams/qYKL0Rbr3uwJ1575964960369.m3u8",
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView( 
        child: Column( 
          children: List.generate(
            listVideo.length, 
            (int index){
              return CardVideo( 
                title: "Video Demo",
                url: listVideo[index],
                height: 180,
                autoPlay: index < 3,
              );
            }
          ),
        ),
      )
    );
  }
}