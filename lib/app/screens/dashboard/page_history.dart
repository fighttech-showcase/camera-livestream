import 'package:cameralivestream/app/ui/components/list_card_video.dart';
import 'package:flutter/material.dart';

class PageHistory extends StatefulWidget {
  @override
  _PageHistoryState createState() => _PageHistoryState();
}

class _PageHistoryState extends State<PageHistory> {
   List<String> listVideo = [
    "https://fighttech.vn/media/video.mp4",
    "https://fighttech.vn/media/video.mp4",
    "https://fighttech.vn/media/video.mp4",
    "https://fighttech.vn/media/video.mp4",
    "https://fighttech.vn/media/video.mp4",
    "https://fighttech.vn/media/video.mp4",
    "https://fighttech.vn/media/video.mp4",
    "https://fighttech.vn/media/video.mp4"
  ];


  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView( 
        child: Column( 
          children: List.generate(
            listVideo.length, 
            (int index){
              return ItemListCardVideo( 
                title: "Video Demo",
                url: listVideo[index],
                height: 120,
              
              );
            }
          ),
        ),
      )
    );
  }
}