import 'package:cameralivestream/app/bloc/bloc_bottom_tab.dart';
import 'package:cameralivestream/app/screens/dashboard/page_history.dart';
import 'package:cameralivestream/app/screens/dashboard/page_home.dart';
import 'package:cameralivestream/app/values/colors.dart';
import 'package:cameralivestream/app/values/dimension.dart';
import 'package:cameralivestream/app/ui/widgets/bottom_bar_ios.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  BottomNavBarBloc _bottomNavBarBloc;
  @override
  void initState() {
    this._bottomNavBarBloc = BottomNavBarBloc();
    super.initState();
  }

  @override
  void dispose() {
    _bottomNavBarBloc?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: SafeArea(
        child: StreamBuilder<NavBarItem>(
          stream: _bottomNavBarBloc.itemStream,
          initialData: NavBarItem.HOME,
          builder: (BuildContext context, AsyncSnapshot<NavBarItem> snapshot) {
            switch (snapshot.data) {
              case NavBarItem.HOME:
                return PageHome();
              case NavBarItem.HISTORY:
                return PageHistory();
              default: 
              return PageHome();
            }
          }
        ),
      ),
      bottomNavigationBar: StreamBuilder(
        stream: _bottomNavBarBloc.itemStream,
        initialData: NavBarItem.HOME,
        builder: (BuildContext context, AsyncSnapshot<NavBarItem> snapshot) {
          return BottomBarIOS(
            size: MySize.ITEM_NAV,
            inactiveColor: MyColor.item_inactive,
            activeColor: MyColor.item_active,
            currentIndex: snapshot.data.index,
            onTap: (index) {
              print("[Dashboard] index: $index");
              _bottomNavBarBloc.pickItem(index);
            },
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.history),
              )
            ]
          );
        }
      )
    );
  }
}