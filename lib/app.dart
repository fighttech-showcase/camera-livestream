import 'package:cameralivestream/app/screens/dashboard.dart';
import 'package:cameralivestream/app/values/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
 
  @override
  Widget build(BuildContext context) {
     SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
 
      // statusBarColor is used to set Status bar color in Android devices.
      statusBarColor: Colors.white,
 
      // To make Status bar icons color white in Android devices.
      statusBarIconBrightness: Brightness.dark,
 
      // statusBarBrightness is used to set Status bar icon color in iOS.
      statusBarBrightness: Brightness.light
      // Here light means dark color Status bar icons.
 
  ));
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        accentColorBrightness: Brightness.light,
        primaryColor: Colors.white,
        accentColor: MyColor.item_active,
        primarySwatch: Colors.lightBlue
      
      ),
      home : DashboardScreen()
    );
  }
}